/* JFlex example: part of Java language lexer specification */
import java_cup.runtime.*;
import java_cup.*;
import supportItems.*;
import symbols.*;
import java.util.*;

%%
%class Lexer
%unicode
%line
%column
%type Token

%eofval{
  return new Token("EOF", Symbols.EOF);
%eofval}

%{
private TableOfSymbols table = new TableOfSymbols();

  private int comments_level = 0;
  StringBuffer string = new StringBuffer();

  private Token token(String value, int type) {

    return new Token(value, type);
}

  private Token insertAndToken(int type, String value ) {
    String lexem = value;

    Token token = new Token(type, lexem);

    java.util.Map.Entry entry = table.addLexem(token.getLessema());
    table.addAttribute(lexem, new Attributo("class", token.getTipo()));

    token.setEntry(entry);
    return token;
}

%}

/*Constants*/
Class = "class"

Break = "break"

If = "if"
Else = "else"
Switch = "switch"
Case = "case"
Default = "default"
Colon = ":"

While = "while"
For = "for"

Public = "public"
Private = "private"
Protected = "protected"
Static = "static"
Final = "final"
Abstract = "abstract"

This = "$this"
Return = "return"


/*Operators*/
Plus = "+"
Multiplication = "*"
Division = "/"
Subtraction = "-"
Increment = "++"
Decrement = "--"
MathOperators = {Plus} | {Multiplication} | {Division} | {Subtraction} | {Increment} | {Decrement}
Assignement = "="

Grpar = "}"
Glpar = "{"
Srpar = "["
Slpar = "]"
Lpar = "("
Rpar = ")"

/* Relational Operators */
LessThen = "<"
GreaterThen = ">"
LessEquals = "<="
GreaterEquals = ">="
EqualEqual = "=="
NotEqual = "!="
RelOp = {LessThen} | {GreaterThen} | {LessEquals} | {GreaterEquals} | {EqualEqual} | {NotEqual}

And = "&&"
Or = "||"
Not = "!"

True = "true"
False = "false"

LineTerminator = \n|;|\r
InputCharacter = [^\r\n]
WhiteSpace = {LineTerminator} | [ \t\f]

StringStart = "\""

/* comments */
TraditionalCommentInit = "/*"
TraditionalCommentEnd = "*/"
TraditionalCommentBody = [^"/*"]*

NoTraditionalComment = {EndOfLineComment} | {DocumentationComment}

CommentContent = ( [^*] | \*+ [^/*] )*
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?
DocumentationComment = "/**" {CommentContent} "*"+ "/"
/*end comments*/
ClassAndMethodName = [A-Za-z][A-Za-z0-9]*

Identifier = "$"((_+[a-zA-Z0-9]+)+_*|([a-zA-Z]+[0-9]*_*)+)
DecIntegerLiteral = 0 | [1-9][0-9]*

%state STRING, COMMENT
%%
<YYINITIAL> {

/* keywords */

{Class}                          { return token(Constants.CLASS, Symbols.CLASS); }

/* Modifiers */
{Private}                        { return token(Constants.PRIVATE, Symbols.PRIVATE); }
{Public}                         { return token(Constants.PUBLIC, Symbols.PUBLIC); }
{Final}                          { return token(Constants.FINAL, Symbols.FINAL); }
{Static}                         { return token(Constants.STATIC, Symbols.STATIC); }
{Protected}                      { return token(Constants.PROTECTED, Symbols.PROTECTED); }
{Abstract}                       { return token(Constants.ABSTRACT, Symbols.ABSTRACT); }

{If}                             { return token(Constants.IF, Symbols.IF); }
{Break}                          { return token(Constants.BREAK, Symbols.BREAK); }
{Else}                           { return token(Constants.ELSE, Symbols.ELSE); }
{True}                           { return token(Constants.TRUE, Symbols.TRUE); }
{False}                          { return token(Constants.FALSE, Symbols.FALSE); }
{While}                          { return token(Constants.WHILE, Symbols.WHILE); }
{Switch}                         { return token(Constants.SWITCH, Symbols.SWITCH); }
{For}                            { return token(Constants.FOR, Symbols.FOR); }
{Case}                           { return token(Constants.CASE, Symbols.CASE); }
{This}                           { return token(Constants.THIS, Symbols.THIS); }
{Default}                        { return token(Constants.DEFAULT, Symbols.DEFAULT); }

{Return}                         { return token(Constants.RETURN, Symbols.RETURN); }

{Colon}                          { return token(Constants.COLON, Symbols.COLON); }

{And}                            { return token(Constants.AND, Symbols.AND); }
{Or}                             { return token(Constants.OR, Symbols.OR); }
{Not}                            { return token(Constants.NOT, Symbols.NOT); }

{Lpar}                           { return token(Constants.LPAR, Symbols.LPAR); }
{Rpar}                           { return token(Constants.RPAR, Symbols.RPAR); }
{Glpar}                          { return token(Constants.GLPAR, Symbols.GLPAR); }
{Grpar}                          { return token(Constants.GRPAR, Symbols.GRPAR); }
{Slpar}                          { return token(Constants.SLPAR, Symbols.SLPAR); }
{Srpar}                          { return token(Constants.SRPAR, Symbols.SRPAR); }


{ClassAndMethodName}             { return insertAndToken(Symbols.CLASSNAME, yytext()); }
{Identifier}                     { return insertAndToken(Symbols.IDENTIFIER, yytext()); }


{DecIntegerLiteral}              { return insertAndToken(Symbols.INTEGER_LITERAL, yytext()); }

{StringStart}                    { string.setLength(0); yybegin(STRING); }

{RelOp}                          { return insertAndToken(Symbols.RELOP, yytext()); }
{MathOperators}                  { return insertAndToken(Symbols.MATH_OP, yytext()); }

{Assignement}                    { return token(Constants.ASSIGNEMENT, Symbols.ASSIGNEMENT); }


{TraditionalCommentInit}         {comments_level ++; yybegin(COMMENT); }
{NoTraditionalComment}           {/*Ignore if is a EndOfLineComment or a DocumentationComment*/}

{LineTerminator}                 { /* ignore */ }
{WhiteSpace}                     { /* ignore */ }
}
<STRING> {

\"                               { yybegin(YYINITIAL); return token( string.toString(), Symbols.STRING_LITERAL); }
[^\n\r\"\\]+                     { string.append( yytext() ); }
\\t                              { string.append('\t'); }
\\n                              { string.append('\n'); }
\\r                              { string.append('\r'); }
\\\"                             { string.append('\"'); }
\\                               { string.append('\\'); }

<<EOF>>                          { throw new Error("Unexpected EOF. \" missed."); }
}

<COMMENT> {

{TraditionalCommentEnd}          { if( -- comments_level == 0) yybegin(YYINITIAL); }
{TraditionalCommentInit}         { comments_level ++; }
{TraditionalCommentBody}         {/*ignore*/}

<<EOF>>                          { throw new Error("Unexpected EOF. /* missed."); }

}

/* error fallback */
[^]                              {System.out.println("Illegal character <"+ yytext()+"> ASCII: " + ((int) yytext().charAt(yytext().length() - 1)) + " " + (yyline + 1) + ":" + (yycolumn + 1));}