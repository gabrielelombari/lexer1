package it.gabrielelombari.compilatori.visitor.syntactic.interfaces;

import it.gabrielelombari.compilatori.visitor.syntactic.element.VisitableNode;

/**
 * Created by Gabriele on 09/01/2017.
 */
public interface IVisitor<T> {

    String visit(VisitableNode<T> vn);
}
