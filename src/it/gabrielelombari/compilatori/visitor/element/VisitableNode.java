package it.gabrielelombari.compilatori.visitor.syntactic.element;

import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;
import it.gabrielelombari.compilatori.visitor.syntactic.interfaces.IVisitor;
import it.gabrielelombari.compilatori.visitor.syntactic.interfaces.Visitable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by Gabriele on 09/01/2017.
 */
public class VisitableNode<T> extends ArrayMultiTreeNode<T> implements Visitable {

    public VisitableNode(T data) {
        super(data);
    }

    @Override
    public String accept(IVisitor v) {
        return v.visit(this);
    }

    @Override
    public void forEach(Consumer<? super TreeNode<T>> action) {
        super.forEach(action);
    }

    @Override
    public Spliterator<TreeNode<T>> spliterator() {
        return super.spliterator();
    }

    @Override
    public Collection<? extends TreeNode<T>> subtrees() {
        if (isLeaf()) {
            return Collections.emptySet();
        }
        Collection<TreeNode<T>> subtrees = new ArrayList<>(subtreesSize);
        for (int i = 0; i < subtreesSize; i++) {
            TreeNode<T> subtree = (TreeNode<T>) this.subtrees[i];
            subtrees.add(subtree);
        }
        return subtrees;
    }

    public VisitableNode<T> getChild(int i) {
        return (VisitableNode<T>) this.subtrees[i];
    }

    public int childsNum() {
        return this.subtreesSize;
    }
}
