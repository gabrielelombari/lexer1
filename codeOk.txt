/**
*Esempio di classe
**/
public class MiaClass{

    public static Main(){
        $var = "testing";
        method($var);
        Pippo();
        LoopMethod();
    }

    //Method method
    public static method($prova){
        $prova = "prova";

        switch($prova){
            case "prova":{
                $prova = "ciao";
                break;
            }

            default:{
                $prova = "di nuovo ciao";
            }
        }
        return $prova;
    }

    public static Pippo(){
        $a = 7;
        $b = 5;

        if($a > $b && $b >= 25 || $a < 10){

            $b = 0;

         }

         for($i = 0; $i < 10; $i ++){
            $i = $i + 1;
         }

         return $b;
    }

    private LoopMethod(){
        while(true);
    }

}